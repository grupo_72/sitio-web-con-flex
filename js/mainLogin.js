// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBMd6nWFVR7E-fsRMCEO6UEVyQNwVb2WKo",
    authDomain: "proyecto-corte-3-1e9ed.firebaseapp.com",
    databaseURL: "https://proyecto-corte-3-1e9ed-default-rtdb.firebaseio.com",
    projectId: "proyecto-corte-3-1e9ed",
    storageBucket: "proyecto-corte-3-1e9ed.appspot.com",
    messagingSenderId: "766959253412",
    appId: "1:766959253412:web:278b98fa60e1c4bea2f331"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnIngresar = document.getElementById('boton');
var btnDesconectar = document.getElementById('btnDesconectar');
var btnLimpiar = document.getElementById('btnLimpiar');

var usuario = "";
var contrasena = "";


const auth = getAuth();


function comprobarDatos(){
    usuario = document.getElementById('usuario').value;
    contrasena = document.getElementById('contrasena').value;
}

// Comprobar usuario y contraseña
if(btnIngresar){
    btnIngresar.addEventListener('click', (e)=>{
        comprobarDatos();
        signInWithEmailAndPassword(auth, usuario, contrasena)
        .then((userCredential) => {
            const user = userCredential.user;
            alert("Ha iniciado sesión exitosamente")
            window.location.href = "/admin.html";
        })
        .catch((error) => {
            alert("Los datos son incorrectos")
            const errorCode = error.code;
            const errorMessage = error.message;
            limpiar();
        });
    });
}

// Cerrar sesión al Administrador
if(btnDesconectar){
    btnDesconectar.addEventListener('click', (e)=>{
        signOut(auth).then(() => {
            alert("Desconexión exitosa")
            window.location.href = "paginaPrincipal.html";
        }).catch((error) => {
            
        })
    })
}


function limpiar(){
    usuario = document.getElementById('usuario');
    contraseña = document.getElementById('contrasena');
    usuario.value = "";
    contrasena.value = "";

}

btnLimpiar.addEventListener('click', limpiar)